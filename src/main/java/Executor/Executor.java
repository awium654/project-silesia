package Executor;

import Bankomat.Atm;
import Calculator.Calculator;
import Homework.WomanNames;

import java.util.Scanner;

public class Executor {
    public static void menu() {
        System.out.println("==========Wybierz aplikacje==========");
        System.out.println("1. Bankomat");
        System.out.println("2. Kalkulator");
        System.out.println("3. Imiona");
        System.out.println("=====================================");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                Atm.atmMenu();
                break;
            case 2:
                Calculator.runCalculator();
                break;
            case 3:
                WomanNames.takeTheName();
                break;
            default:
                System.out.println("Wybrana funkcja nie istnieje!\n");
                menu();
                break;
        }
    }
}
