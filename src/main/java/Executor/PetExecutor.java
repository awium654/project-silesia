package Executor;
import Pet.Pet;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class PetExecutor {
    private static Scanner scanner = new Scanner(System.in);
    private static List<Pet> petList = new ArrayList();
    private static double balance = 1000.0;
    private static int count = 0;

    public static void menuPet() {
        System.out.println("==========PET STORE==========");
        System.out.println("0. Początkowy bilans firmy to: 1000.0");
        System.out.println("=============MEMU=============");
        System.out.println("1. Dodaj zwierzaka");
        System.out.println("2. Sprzedaj zwierzaka");
        System.out.println("3. Lista zwierzaków na sprzedaż");
        System.out.println("4. Wyszukiwarka");
        System.out.println("5. Wyświetl bilans sklepu");
        System.out.println("6. Wyjście");

        int choice = scanner.nextInt();
        scanner.nextLine();

        try {
            if (choice == 1) {
                addPet();
            } else if (choice == 2) {
                sellPet();
            } else if (choice == 3) {
                vievListPets();
            } else if (choice == 4) {
                menuSearch();
            } else if (choice == 5) {
                balanceShop();
            } else if (choice == 6) {
                System.out.println("Zamykam program");
            }
        } catch (Exception e) {
            System.out.println("Wybrałeś niepoprawną opcję!\n");
            menuPet();
        }
    }

    private static void vievListPets() {
        if (petList.size() != 0) {
            for (int i = 0; i < petList.size(); i++) {
                Pet petWithList = petList.get(i);
                System.out.println("Nr UUID: " + petWithList.getUuid() +
                        "\nGatunek: " + petWithList.getGatunek() +
                        "\nKolor: " + petWithList.getKolor() +
                        "\nCena: " + petWithList.getCena() +
                        "\n====================================");
            }
        } else {
            System.out.println("Sklep wyprzedany!");
        }
        menuPet();
    }

    private static void addPet() {
        Pet pet = new Pet();
        System.out.println("==========Dodawanie zwierzaka==========");
        System.out.println("Podaj gatunek: ");
        String type = scanner.nextLine();

        System.out.println("Podaj kolor: ");
        String color = scanner.nextLine();

        System.out.println("Podaj cenę: ");
        float price = scanner.nextInt();

        pet.setUuid(UUID.randomUUID().toString());
        pet.setType(type);
        pet.setColor(color);
        pet.setPrize(price);
        System.out.println("Dodałeś zwierze! " + type.toUpperCase());

        petList.add(pet);
        menuPet();
    }

    private static void balanceShop() {
        System.out.println("Aktualny bilans sklepu: " + balance);
        menuPet();
    }

    private static void menuSearch() {
        System.out.println("1. Wyszukuj po UUID");
        System.out.println("2. Wyszukuj po Gatunku");
        System.out.println("3. Wyszukuj po Kolorze");
        System.out.println("4. Wyszukuj po Cenie mniejszej");
        System.out.println("5. Cofnij do Menu Pet");

        int search = scanner.nextInt();
        switch (search) {
            case 1:
                searchNrUUID();
                break;
            case 2:
                searchNamePet();
                break;
            case 3:
                searchColorPet();
                break;
            case 4:
                searchPrizePet();
                break;
            case 5:
                menuPet();
                break;
            default:
                System.out.println("Brak opcji! ");
                menuPet();
                break;
        }
    }

    private static void searchPrizePet() {
        System.out.println("Podaj cene maksymalną: ");
        double prize = scanner.nextDouble();
        scanner.nextLine();
        for (int i = 0; i < petList.size(); i++) {
            if (petList.get(i).getCena() <= prize) {
                soutPet(i);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak zwierzaka w podanym zakresie.");
        }
        count = 0;
        menuSearch();
    }

    private static void searchColorPet() {
        System.out.println("Podaj kolor zwierzaka: ");
        String color = scanner.next();
        scanner.nextLine();
        for (int i = 0; i < petList.size(); i++) {
            if (petList.get(i).getKolor().equalsIgnoreCase(color)) {
                soutPet(i);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak zwierzaka o podanym kolorze.");
        }
        count = 0;
        menuSearch();
    }

    private static void soutPet(int i) {
        System.out.println("Znazłem: \n" + petList.get(i).getUuid() +
                "\nGatunek: " + petList.get(i).getGatunek() +
                "\nKolor: " + petList.get(i).getKolor() +
                "\nCena: " + petList.get(i).getCena() +
                "\n====================================");
    }

    private static void searchNamePet() {
        System.out.println("Podaj nazwę Gatunku zwierzaka: ");
        String name = scanner.next();
        scanner.nextLine();
        for (int i = 0; i < petList.size(); i++) {
            if (petList.get(i).getGatunek().equalsIgnoreCase(name)) {
                soutPet(i);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak zwierzaka o podaj nazwię.");
        }
        count = 0;
        menuSearch();
    }

    private static void searchNrUUID() {
        System.out.println("Podaj nr UUID: ");
        String nrUUID = scanner.next();
        scanner.nextLine();
        for (int i = 0; i < petList.size(); i++) {
            if (petList.get(i).getUuid().equalsIgnoreCase(nrUUID)) {
                soutPet(i);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak nr UUID!");
        }
        count = 0;
        menuSearch();
    }

    private static void sellPet() {
        System.out.println("Podaj nr uuid zwierza: ");
        String nrUUID = scanner.next();
        scanner.nextLine();
        for (int i = 0; i < petList.size(); i++) {
            if (petList.get(i).getUuid().equals(nrUUID)) {
                balance += petList.get(i).getCena();
                System.out.println("Kupiłeś zwierzaka: " + petList.get(i).getGatunek().toUpperCase());
                petList.remove(petList.get(i));
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak podanego nr UUID!");
        }
        count = 0;
        menuPet();
    }
}