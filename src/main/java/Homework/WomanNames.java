package Homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WomanNames {
    public static Scanner name = new Scanner(System.in);

    public static void takeTheName() {
        System.out.println("Podaj cyfrę: ");
        List<String> namesList = new ArrayList<>();
        List<String> resList = new ArrayList<>();
        int nameNum = name.nextInt();
        name.nextLine();
        System.out.println("Podaj " + nameNum + " imion kobiet! Po każdym imieniu naciśnij ENTER!!");
        while (namesList.size() != nameNum) {
            String womanName = name.nextLine();
            namesList.add(womanName);
        }
        char endOfName = 'a';
        for (int i = 0; i < namesList.size(); i++) {
            String theName = namesList.get(i);
            int sizeName = theName.length() - 1;
            char charLastName = theName.charAt(sizeName);
            if (charLastName == endOfName) {
                resList.add(theName);
            }
        }
        System.out.println("Pokazuje tylko te co kończą się na 'a' !");
        for (String name : resList) {
            System.out.print(name + ", ");
        }
    }
}

