package Pet;


import java.util.Objects;

public class Pet {
    private String uuid;
    private String gatunek;
    private String kolor;
    private float cena;

    public Pet() {
    }

    public Pet(String uuid, String gatunek, String kolor, float cena) {
        this.uuid = uuid;
        this.gatunek = gatunek;
        this.kolor = kolor;
        this.cena = cena;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getGatunek() {
        return gatunek;
    }

    public void setType(String gatunek) {
        this.gatunek = gatunek;
    }

    public String getKolor() {
        return kolor;
    }

    public void setColor(String kolor) {
        this.kolor = kolor;
    }

    public double getCena() {
        return cena;
    }

    public void setPrize(float cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "uuid='" + uuid + '\'' +
                ", gatunek='" + gatunek + '\'' +
                ", kolor='" + kolor + '\'' +
                ", cena=" + cena +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Float.compare(pet.cena, cena) == 0 &&
                uuid.equals(pet.uuid) &&
                gatunek.equals(pet.gatunek) &&
                kolor.equals(pet.kolor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, gatunek, kolor, cena);
    }
}
