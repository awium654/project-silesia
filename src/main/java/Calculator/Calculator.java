package Calculator;

import java.util.Scanner;

public class Calculator {

    public static void runCalculator() {
        Calculator calc = new Calculator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write a program calculator");
        System.out.println("=============Calculator.Calculator==========");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Division");
        System.out.println("4. Multiplication");
        System.out.println("===================================");
        int choice = scanner.nextInt();
        if (choice == 1) {
            System.out.println("============Adding selected==========");
            System.out.println("Enter the first number: ");
            int first = scanner.nextInt();
            System.out.println("Enter the second number: ");
            int second = scanner.nextInt();
            System.out.println("The result of adding it: " + calc.add(first, second));
        } else if (choice == 2) {
            System.out.println("============Subtraction selected==========");
            System.out.println("Enter the first number: ");
            int first = scanner.nextInt();
            System.out.println("Enter the second number: ");
            int second = scanner.nextInt();
            System.out.println("The result of subtraction it: " + calc.subtraction(first, second));
        } else if (choice == 3) {
            System.out.println("============Division selected==========");
            System.out.println("Enter the first number: ");
            double first = scanner.nextDouble();
            System.out.println("Enter the second number: ");
            double second = scanner.nextDouble();
            System.out.println("The result of division it: " + calc.division(first, second));
        } else if (choice == 4) {
            System.out.println("============Multiplication selected==========");
            System.out.println("Enter the first number: ");
            int first = scanner.nextInt();
            System.out.println("Enter the second number: ");
            int second = scanner.nextInt();
            System.out.println("The result of multiplication it: " + calc.multiplication(first, second));
        }
    }
    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtraction(int a, int b) {
        return a - b;
    }

    public static double division(double a, double b) {
        return a / b;
    }

    public static int multiplication(int a, int b) {
        return a * b;
    }
}
