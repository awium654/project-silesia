package Bankomat;

import java.util.Scanner;

public class Atm {
    private static float balance = 100f;
    private static Scanner scanner = new Scanner(System.in);

    public static void atmMenu() {
        System.out.println("===========MENU==========");
        System.out.println("1. Sprawdź środki");
        System.out.println("2. Wpłać pieniądze");
        System.out.println("3. Wypłać pieniądze");
        System.out.println("4. Wyjdź");
        System.out.println("=========================");

        int choice = scanner.nextInt();

        try {
            if (choice == 1) {
                checkBalance();
            } else if (choice == 2) {
                deposited();
            } else if (choice == 3) {
                withdraw();
            } else if (choice == 4) {
                System.out.println("Dziękujemy i zapraszamy ponownie!");
            } else {
                System.out.println("Wprowadzono niewłaściwą wartość!");
                atmMenu();
            }
        } catch (Exception e) {
            System.out.println("Wprowadzono niewłaściwą wartość!!");
        }
    }

    private static void checkBalance() {
        System.out.println("Twoje saldo to: " + balance + "\n");
        atmMenu();
    }

    private static void deposited() {
        System.out.println("Wprowadź kwotę do wpłacenia: ");
        float payment = scanner.nextFloat();
        if (payment > 0) {
            balance += payment;
            System.out.println("Wpłacono: " + payment + "\n");
        }
        atmMenu();
    }

    private static void withdraw() {
        System.out.println("Wprowadź kwotę do wypłacenia: ");
        float withdraw = scanner.nextFloat();
        if (withdraw > 0 && withdraw <= balance ) {
            balance -= withdraw;
            System.out.println("Wypłacono: " + withdraw + "\n");
        } else {
            System.out.println("Brak wystarczających środków na koncie!" +
                    " Dostępne saldo to " + balance + "\n");
        }
        atmMenu();
    }
}
